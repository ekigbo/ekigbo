# ekigbo

[[_TOC_]]

## About me

- 📍 Melbourne, Australia (UTC+10/+11 depending on DST)
- ☕️ Filter coffee
- ⚽️ Football / 🚴 Cycling / 🎸 Guitar / 🎹 Synthesizers
- 🎵 Mostly Metal/Jazz/Noise/Experimental - i like what i like, i dont judge
- 💻 Sr Frontend Engineer
- 📈 Plan stage, Optimize team

## Working style 

I prefer working async, pair programming + unplanned / ad hoc sync work is usually more stressful and less efficient for me.

### Values

Our [Collaboration value](https://handbook.gitlab.com/handbook/values/#collaboration) resonates the strongest with me, it guides how I work and how I hope to interact with everyone both at GitLab and outside.

Some particular sub values that resonate / im working on:
  - [Kindness](https://handbook.gitlab.com/handbook/values/#kindness)
  - [Assume positive intent](https://handbook.gitlab.com/handbook/values/#assume-positive-intent)
  - [Bias for action](https://handbook.gitlab.com/handbook/values/#operate-with-a-bias-for-action)
  - [Boring solutions](https://handbook.gitlab.com/handbook/values/#boring-solutions)
  - [Bias towards asynchronous communication](https://handbook.gitlab.com/handbook/values/#bias-towards-asynchronous-communication)
  - [Seek diverse perspectives](https://handbook.gitlab.com/handbook/values/#seek-diverse-perspectives)
  - [Quirkiness](https://handbook.gitlab.com/handbook/values/#quirkiness)
  - [Minimal viable change](https://handbook.gitlab.com/handbook/values/#minimal-viable-change-mvc)
  - [Low level of shame](https://handbook.gitlab.com/handbook/values/#low-level-of-shame)
  - [Public by default](https://handbook.gitlab.com/handbook/values/#public-by-default)
  - [Findability](https://handbook.gitlab.com/handbook/values/#findability)

### Schedule

I often work strange hours, so dont be surprised if you see me pop up randomly. I like to balance starting and ending my day with MR reviews, the majority of my productive feature work happens Tuesdays - Thursdays.

If you need to schedule a meeting with me, my first preference is generally a Tuesday / Wednesday [Melbourne time](https://www.timeanddate.com/worldclock/australia/melbourne), otherwise find a spot and i'll get back to you 🙂. 

I really enjoy coffee chats ☕️, so feel free to schedule one.

## Mentoring
I genuinely enjoy mentoring others and it is a skill I want to continue to develop. I've mentored formally and informally within and outside engineering on:
  - Becoming a maintainer
  - Interview training
  - General life @ GitLab

Dont hesitate to reach out if you think we could be a mentor/mentee fit